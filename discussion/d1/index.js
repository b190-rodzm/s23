// console.log("hi");

// let cellphone = {
//   name: "Nokia 3210",
//   manufacturedDate: 1999
// };
// console.log(`Result from creating objects using initializers`);
// console.log(cellphone);
// console.log(typeof cellphone);

let car = {
  brand: ["Tesla", "Honda"],
  model: ["Model S","Civic"],
  yearReleased: [2012,2000],
  display: function(index){
    console.log(this.brand[index]);
    console.log(this.model[index]);
    
  }
}
car.display(1)


let person = {
  name: 'John',
  talk: function (){
    console.log(`Hello! My name is ${this.name}.`);
  },
  walk: function (){
    console.log(`${this.name} walked 25 steps`);
  }
}
person.walk()

let friend = {
	firstName: "Joe",
	lastName: "Smith",
	// nested object
	address: {
		city: "Austin",
		state: "Texas"
	},
	// nested array
	emails: [ 'joe@mail.com', "johnHandsome@mail.com" ],
	introduce: function(){
		console.log("Hello! My name is " + this.firstName + " " + this.lastName)
	}
}
friend.introduce();
console.log(friend);


let myPokemon = {
  pkmn: 'Pikachu',
  level: 5,
  health: 100,
  attack: 50,
  tackle: function (){
    console.log (`${pkmn} used Tackle against targetPokemon.`)
    console.log(`targetPokemon's health is reduced by to targetPkmnHealh`);
  },
  faint: function(){
    console.log("enemyPokemon fainted.");
  }
}
console.log(myPokemon);

function Pokemon (name, level){
  this.name = name;
  this.level = level;
  this.health = 2 * level;
  this.attack = level;

  this.tackle = function (target){
    console.log(this.name + " tackled " + target.name);
  }
  this.faint = function(){
    console.log(this.name + "fainted.");
  }
}

let bulbasaur = new Pokemon ("Bulbasaur", 12);
let charmander = new Pokemon ("Charmander", 17);


bulbasaur.tackle(charmander);




// function Laptop (name, manufactureYear){
//   this.name = name;
//   this.manufactureYear = manufactureYear;
// }
// let laptop = new Laptop ("Dell", 2012);
// console.log("object by initializer");
// console.log(laptop);
// console.log(typeof laptop);

// let laptop2 = new Laptop ("Lenovo", 2008);
// console.log(laptop2);

// let computer = {};
// let myComputer = new Object();
// console.log("using initializer");
// console.log(computer);
// console.log(typeof myComputer);