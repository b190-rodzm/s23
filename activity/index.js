console.log('\x1B[31mPokemon\x1B[34m Brawl');


let trainer = {
  name: "Ash",
  age: 12,
  pokemon: ["Pikachu", "Bulbasaur", "Charmander", "Squirtle"],
  friends: {
    kanto: ["Misty", "Brock"],
    hoenn: ["Max", "Misty"]}
};
trainer.talk = function (){
  console.log("Pikachu, I choose you!");
}
console.log(trainer);
console.log(`You Pokemon trainer name is:`);
console.log(trainer.name);
console.log(`Your starting Pokemon is:`);
console.log(trainer["pokemon"]);
trainer.talk()

// POKEMON BRAWL //

// Items
let sitrusBerry = 10;

function Pokemon (pkmn, level, move1, move2){
  this.pkmn = pkmn;
  this.level = level;
  this.health = 4 * level;
  this.fullHealth = this.health;
  this.power = level / 2;
  this.move1 = move1;
  this.move2 = move2;
  this.exp = 0;
  this.maxExp = level ** 3;

  preCounter = function(enemy){
    console.log(`${enemy.pkmn} is preparing to counter-attack...`);
  }
  counter = function(enemy){
    let random = Math.trunc(Math.random() * 10) + 1; 
    console.log(`${enemy.pkmn} used ${enemy.move1}!`);
    if (random <= 1) {
      console.log(`but ${enemy.pkmn}'s attack missed!`);
      console.warn(this.pkmn,"Lv",this.level, "HP:", this.health,"/",this.fullHealth, "|", enemy.pkmn,"Lv",enemy.level, "HP:", enemy.health,"/",enemy.fullHealth);
    } else if (random > 1 && random <=3) {
      console.log(`but ${this.pkmn} evaded the attack!`);
      console.warn(this.pkmn,"Lv",this.level, "HP:", this.health,"/",this.fullHealth, "|", enemy.pkmn,"Lv",enemy.level, "HP:", enemy.health,"/",enemy.fullHealth);
    }
    else if (random > 3 && random <9) { 
      this.health = this.health - enemy.power; 
      if (this.health > 0){ 
        console.log(`${this.pkmn} took some damage.`);
        console.log(`${this.pkmn}'s HP is reduced to ${this.health}.`);
        console.warn(this.pkmn,"Lv",this.level, "HP:", this.health,"/",this.fullHealth, "|", enemy.pkmn,"Lv",enemy.level, "HP:", enemy.health,"/",enemy.fullHealth);
      } else {
        enemy.faint (this);
        console.error(this.pkmn,"Lv",this.level, "HP:", 0,"/",this.fullHealth, "|", enemy.pkmn,"Lv",enemy.level, "HP:", enemy.health,"/",enemy.fullHealth)
      }
    } else { 
      this.health = this.health - (enemy.power)*2;
      if (this.health > 0){
        console.log('%cA critical hit!','font-weight: bold');
        console.log(`${this.pkmn} took a lot of damage!`);
        console.log(`${this.pkmn}'s HP is reduced to ${this.health}.`);
        console.warn(this.pkmn,"Lv",this.level, "HP:", this.health,"/",this.fullHealth, "|", enemy.pkmn,"Lv",enemy.level, "HP:", enemy.health,"/",enemy.fullHealth);
      } else {
        enemy.faint(this);
        console.error(this.pkmn,"Lv",this.level, "HP:", 0,"/",this.fullHealth, "|", enemy.pkmn,"Lv",enemy.level, "HP:", enemy.health,"/",enemy.fullHealth)
      }}
  };
 
  this.attack = function (enemy){
    if (this.health <= 0){
      console.error(`${this.pkmn} can no longer attack.`);
    } else {
      if (enemy.health <= 0) { 
        console.error(`You cannot attack a fainted Pokemon!`);
      } else {
        //main attack
        let random = Math.trunc(Math.random() * 10) + 1; 
        console.log("\x1B[34mGo "+this.pkmn + "!");
        console.log(`${this.pkmn} attacks ${enemy.pkmn}...`);
        console.log(`${this.pkmn} used ${this.move1}!`);
        if (random <= 1) {
          console.log(`but ${this.pkmn}'s attack missed!`);
          console.warn(this.pkmn,"Lv",this.level, "HP:", this.health,"/",this.fullHealth, "|", enemy.pkmn,"Lv",enemy.level, "HP:", enemy.health,"/",enemy.fullHealth);
          setTimeout(preCounter.bind(this), 2900, enemy);
          setTimeout(counter.bind(this), 5900, enemy);
          // this.counter (enemy);
  
        } else if (random > 1 && random <=3) {
          console.log(`but ${enemy.pkmn} evaded the attack!`);
          console.warn(this.pkmn,"Lv",this.level, "HP:", this.health,"/",this.fullHealth, "|", enemy.pkmn,"Lv",enemy.level, "HP:", enemy.health,"/",enemy.fullHealth);
          setTimeout(preCounter.bind(this), 2900, enemy);
          setTimeout(counter.bind(this), 5900, enemy);
          // this.counter (enemy);

        } else if (random > 3 && random <9) { 
          enemy.health = enemy.health - this.power; 
          if (enemy.health > 0){ 
            console.log(`${enemy.pkmn} took some damage.`);
            console.log(`${enemy.pkmn}'s HP is reduced to ${enemy.health}.`);
            console.warn(this.pkmn,"Lv",this.level, "HP:", this.health,"/",this.fullHealth, "|", enemy.pkmn,"Lv",enemy.level, "HP:", enemy.health,"/",enemy.fullHealth);
            setTimeout(preCounter.bind(this), 2900, enemy);
            setTimeout(counter.bind(this), 5900, enemy);
          // this.counter (enemy); 
          } else {
            this.faint (enemy);
            console.error(this.pkmn,"Lv",this.level,  "HP:", this.health,"/",this.fullHealth, "|", enemy.pkmn,"Lv",enemy.level, "HP:", 0,"/",enemy.fullHealth)
          }
        } else { 
          enemy.health = enemy.health - (this.power)*2;
          if (enemy.health > 0){
            console.log('%cA critical hit!','font-weight: bold');
            console.log(`${enemy.pkmn} took a lot of damage!`);
            console.log(`${enemy.pkmn}'s HP is reduced to ${enemy.health}.`);
            console.warn(this.pkmn,"Lv",this.level, "HP:", this.health,"/",this.fullHealth, "|", enemy.pkmn,"Lv",enemy.level, "HP:", enemy.health,"/",enemy.fullHealth);
            setTimeout(preCounter.bind(this), 2900, enemy);
            setTimeout(counter.bind(this), 5900, enemy);
          } else {
            this.faint(enemy);
            console.error(this.pkmn,"Lv",this.level,  "HP:", this.health,"/",this.fullHealth, "|", enemy.pkmn,"Lv",enemy.level, "HP:", 0,"/",enemy.fullHealth)
          }
        };

      }
    }
  };

  this.specialAttack = function (enemy){
    if (this.health <= 0){
      console.error(`${this.pkmn} can no longer attack.`);
    } else {
      if (enemy.health <= 0) { 
        console.error(`You cannot attack a fainted Pokemon!`);
      } else {
        //main attack
        let random = Math.trunc(Math.random() * 10) + 1; 
        console.log("\x1B[34mGo "+this.pkmn + "!");
        console.log(`${this.pkmn} attacks ${enemy.pkmn}...`);
        console.log(`${this.pkmn} used ${this.move2}!`);
        if (random <= 1) {
          console.log(`but ${this.pkmn}'s attack missed!`);
          console.warn(this.pkmn, "HP:", this.health,"/",this.fullHealth, "|", enemy.pkmn, "HP:", enemy.health,"/",enemy.fullHealth);
          setTimeout(preCounter.bind(this), 2900, enemy);
          setTimeout(counter.bind(this), 5900, enemy);
        } else if (random > 1 && random <=3) {
          console.log(`but ${enemy.pkmn} evaded the attack!`);
          console.warn(this.pkmn, "HP:", this.health,"/",this.fullHealth, "|", enemy.pkmn, "HP:", enemy.health,"/",enemy.fullHealth);
          setTimeout(preCounter.bind(this), 2900, enemy);
          setTimeout(counter.bind(this), 5900, enemy);
        }
        else if (random > 3 && random <9) { 
          enemy.health = enemy.health - this.power; 
          if (enemy.health > 0){ 
            console.log(`${enemy.pkmn} took some damage.`);
            console.log(`${enemy.pkmn}'s HP is reduced to ${enemy.health}.`);
            console.warn(this.pkmn, "HP:", this.health,"/",this.fullHealth, "|", enemy.pkmn, "HP:", enemy.health,"/",enemy.fullHealth); 
            setTimeout(preCounter.bind(this), 2900, enemy);
            setTimeout(counter.bind(this), 5900, enemy);
          } else {
            this.faint (enemy);
            console.error(this.pkmn, "HP:", this.health,"/",this.fullHealth, "|", enemy.pkmn, "HP:", 0,"/",enemy.fullHealth)
          }
        } else { 
          enemy.health = enemy.health - (this.power)*2;
          if (enemy.health > 0){
            console.log('%cA critical hit!','font-weight: bold');
            console.log(`${enemy.pkmn} took a lot of damage!`);
            console.log(`${enemy.pkmn}'s HP is reduced to ${enemy.health}.`);
            console.warn(this.pkmn, "HP:", this.health,"/",this.fullHealth, "|", enemy.pkmn, "HP:", enemy.health,"/",enemy.fullHealth)
            setTimeout(preCounter.bind(this), 2900, enemy);
            setTimeout(counter.bind(this), 5900, enemy);
          } else {
            this.faint(enemy);
            console.error(this.pkmn, "HP:", this.health,"/",this.fullHealth, "|", enemy.pkmn, "HP:", 0,"/",enemy.fullHealth)
          }
        };

      }
    }
  };

  this.faint = function (enemy) {
      console.log(`${enemy.pkmn}'s HP is reduced to 0.`);
      console.log("\x1B[31m"+enemy.pkmn + " fainted!");
      this.gainExp(enemy);
  };

  this.levelUp = function (enemy){
    this.exp = this.exp - this.maxExp;
    this.level++;
    this.maxExp = this.level ** 3;
    console.log(`${this.pkmn} leveled up!`);
    if(this.exp > this.maxExp){
      this.levelUp(enemy);
    } else {
      console.warn("EXP ",this.exp,"/",this.maxExp, "  Lv", this.level);

    }
  };

  this.gainExp = function (enemy){
    let expGain = enemy.level ** 2;
    console.log("\x1B[34m"+this.pkmn + "\x1B[34m gained " + expGain+ "\x1B[34m EXP!");
    this.exp = this.exp + expGain;
    if (this.exp > this.maxExp) { // fills up EXP bar
      this.levelUp(enemy);
      }  
    else {
      this.exp = this.exp + expGain;
      console.warn(this.pkmn,"Lv", this.level, "HP: ",this.health," EXP:",this.exp,"/",this.maxExp);
      // do nothing
  }
  };

  this.heal = function (){
    if (sitrusBerry === 0){
      console.log("No more berries left.");
    } else {
      sitrusBerry--;
      this.health = this.health + this.fullHealth/4;
      if (this.health >= this.fullHealth) {
        this.health = this.fullHealth
        console.log(`You gave ${this.pkmn} a Sitrus Berry.`);
        console.log(`${this.pkmn} regained health.`);
        console.warn(this.pkmn, "HP:", this.health,"/",this.fullHealth, " | ", "Berries left: ", sitrusBerry);
      } else {
        console.log(`You gave ${this.pkmn} a Sitrus Berry.`);
        console.log(`${this.pkmn} regained health.`);
        console.warn(this.pkmn, "HP:", this.health,"/",this.fullHealth, " | ", "Berries left: ", sitrusBerry);
      }
    }
  };
};


  
// ÷÷÷÷÷ Pokemon Brawlers List÷÷÷÷÷

  let pikachu = new Pokemon ("Pikachu", 11, "Quick Attack", "Thunder Shock");
  let bulbasaur = new Pokemon ("Bulbasaur", 13, "Tackle", "Vine Whip");
  let squirtle = new Pokemon ("Squirtle", 11, "Tackle", "Bubble");
  let charmander = new Pokemon ("Charmander", 12, "Scratch", "Ember");
  let rattata = new Pokemon ("Rattata", 12, "Scratch", "Bite");


// Legendary Pokemon list
let rayquaza = new Pokemon ("Rayquaza", 99, "Outrage", "Hyper Beam");
let arceus = new Pokemon ("Arceus", 99, "Seismic Toss", "Hyper Beam");

